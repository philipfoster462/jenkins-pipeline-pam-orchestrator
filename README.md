# Immutable KIE Server Openshift Deployment Pipeline
<!-- This project will deploy a KJAR into an immutable KIE server. -->
This project provides the tooling necesary to build and deploy a Red Hat Process Automation Manager (RHPAM) project into an immutable Kie server on Openshift.

To build the Docker image, an BuildConfig will run an [S2I (Source to Image)](https://github.com/openshift/source-to-image) build and deploy the built Docker image into a docker registry such as Quay. 
A Jenkinsfile is included to create a pipeline which will automate the process of invoking a build with the BuildConfig. 

To deploy the docker image, a DeploymentConfig will pull the latest image from the docker registry, and automatically perform a rolling deployment for a seamless update process.
A separate Jenkins file is included to automate the update process. 

The build and deployment processes are strictly separated, and follow best practices for a [12-factor application](https://12factor.net/). 

<a href="#prerequisites"></a>
## Prerequisites
- Openshift 3.11 cluster
- Enterprise Git server (ex. Github)
- Existing RHPAM project
- External RDBMS (see list of [supported databases](https://access.redhat.com/articles/3405381))
- Java JDK 8+
- Jenkins
- Maven
- Docker registry ([Quay](https://quay.io/) used in this README, but any should work)

Ensure that your openshift cluster has the RHPAM 7.7 image streams in the `openshift` namespace. 
If not, run `oc apply -f env-setup/rhpam77-image-streams.yaml -n openshift` to add them. Your cluster will need to have a service account setup with the Red Hat registry.  If this is necessary, an alert with documentation information will pop up under the created image streams in the Openshift web console. 

Additionally, you will need to setup and install the Openshift Jenkins plugin. Instructions to do this are included in the plugin github repository [here](https://github.com/openshift/jenkins-client-plugin#overview).


Create a user and database in your external RDBMS. Note these fields for later. 

### Notes on Configuring Jenkins Openshift plugin

The Jenkins pipelines assume that Jenkins is configured in a certain way. It should not be difficult to change this opinionated setup to fit an existing environment, however. It is assumed that you have separate dev, test, staging, and prod environments, each with their own Openshift cluster. When setting up the Openshift clusters  in the Openshift client plugin (Jenkins --> Manage Jenkins --> Configure System --> Openshift Client Plugin), each environment will have an entry. Cluster name should indicate which environment you are in. Make a note of the configured cluster names here. If they are different from 'dev'; 'test'; 'stage'; 'prod', then you will need to make a small change to your Jenkinsfiles.

Open the `build/Jenkinsfile` file in your favorite text editor. By default, all builds will be done in the `dev` environment. If you would rather use another environment or use a different name, find the line `openshift.withCluster("dev")`, and change "dev" to match the environment you want builds to be run in.

Open the `deploy/Jenkinsfile` file in your favorite text editor. Notice the `DEPLOY_ENV` choice paramter. Change the choices here to match
the cluster names you have configured in Jenkins.

<!-- TODO: Investigate if there's a better/more secure/self-service way to do this...-->
<!-- Maybe grant system:build-controller, system:deployer, and system:openshift:controller:deploymentconfig-controller cluster roles to jenkins SA? https://docs.openshift.com/enterprise/3.0/admin_guide/manage_authorization_policy.html -->
You will also need to have a service account provisioned for the Jenkins on Openshift. This will likely require assistance from your cluster administrator. 

`oc project my-decisionservice`
`oc policy add-role-to-user admin system:serviceaccount:openshift:jenkins`


<a href="#ocp-project-setup"></a>

## Initial project setup
*In the following steps, you will likely be making a few minor changes to this repo to fit your environment. Before getting started, fork this into your own git repo so that your changes can be saved.*

First, login to the openshift console with `oc login`. 
Create a new project `oc new-project my-decisionservice`
Ensure that you are using the project by running `oc project`. If not, switch to the newly created project with `oc project my-decisionservice`. 
 - *Note: In this case, we're creating a project 'my-decisionservice'. Naturally, you should use a name that is relevant to your project and adheres to any established naming standards in your organisation. Future references to `my-decisionservice` in this documentation should be replaced with the name you chose*


Next, login to Quay and navigate to your organization page. Click the "Create new repository" button in the top right. Add a repository name to match your project name, set the desired visibility level, and make sure "Empty repository" is selected. Click "Create [Private|Public] repository". Navigate back to your organization page and select the "Robot Accounts" tab. Click "Create robot account" and fill in the requested details in the dialog box. Best practice is to make sure that the repository name matches the OCP project name as closely as possible. After filling out the dialog, click "Create robot account". Set the robot account permission level to "Write" for the repository you just created. After the account is created, find the robot account from the list, and click it. Click "Kubernetes secret" --> "Download [secret-file-name].yml" to download the secret. Navigate to your downloads directory in a terminal session, and add it to Openshift with `oc apply -f [secet-file-name].yml -n my-decisionservice`
 - *Note: The specific steps to setup a repository and add a service account vary depending on your docker reigstry of choice. Consult your registry documentation for specific details. The secret created is a dockerconfigjson file*


Next, modify the secrets in the `env-setup/secrets.yaml` to have a signed (if applicable) SSL keystore in .jks format and strong KIE server admin username and password. All secrets in the file should be formatted as base64 strings. To get the keystore in base64 format, you can use `cat keystore.jks | base64` in a bash terminal. You can now add the secrets to Openshift with the command `oc apply -f env-setup/secrets.yaml -n my-decisionservice`



Open `env-setup/vars/build.properties` in a text editor. Modify the parameters there to match your environment. Full documentation for the required params is available through via command `oc process --parameters -f env-setup/build.yaml`
- `APPLICATION_NAME` should be the name of the project (`my-decisionservice` in this case)
- `KIE_SERVER_CONTAINER_DEPLOYMENT` is the KIE deplyment config and alias. Should match your project kjar GAV. 
- `SOURCE_REPOSITORY_URL` is the a link to your RHPAM project repo to build and deploy. 
- `EXTERNAL_DOCKER_REPO_URL` is the link to your docker repository to push and pull from. In quay, you can get this link by clicking the copy button next to the docker pull command in the bottom right of your repository page and pasting it into this variable. If the leading `docker pull` got copied, remove that part.
- `DOCKER_PUSH_PULL_SECRET` is the quay (or other repo) secret created earlier. Should match `[secret-file-name]` that you downloaded earlier if using quay. If unsure, you can check the options with `oc get secrets`

Next, create the build configuration with `oc process -f build.yaml --param-file=vars/build.properties | oc create -f -`. Run `oc logs bc/my-decisionservice-kieserver.yaml -f`. Notice that this triggered an initial maven build in Openshift in addition to creating a BuildConfig object. Going forward, this build config will be used to build our immutable KIE server image and push. Once the build is completed, navigate to your docker repository. Click the 'Tags' tab, and refresh if necessary. You should see a new build with the 'latest' tag. This indicates tha your built image was pushed successfully to the external repository.
- *Adding a BuildConfig should only be done in the environment you have designated to run builds (dev by default)*



Now, we can get ready to deploy the rest of the project. Similarly to the `build.properties` file you edited earlier, open `env-setup/vars/kieserver.properties` in a text editor. Ensure that any properties you modified in `build.properties` match EXACTLY in this file as well. 
- `APPLICATION_NAME` should be the name of the project (`my-decisionservice` in this case)
- `KIE_SERVER_CONTAINER_DEPLOYMENT` is the KIE deplyment config and alias. Should match your project kjar GAV. 
- `EXTERNAL_DOCKER_REPO_URL` is the link to your docker repository to push and pull from. In quay, you can get this link by clicking the copy button next to the docker pull command in the bottom right of your repository page and pasting it into this variable. If the leading `docker pull` got copied, remove that part.
- `DOCKER_PUSH_PULL_SECRET` is the quay (or other repo) secret created earlier. Should match `[secret-file-name]` that you downloaded earlier if using quay. If unsure, you can check the options with `oc get secrets`
- `KIE_SERVER_EXTERNALDB_*` - Various configuration parameters to your external database. 


You can now deploy the rest of the KIE server in the same way as before.
`oc process -f env-setup/rhpam77-prod-immutable-kieserver.yaml --param-file=env-setup/vars/kieserver.properties | oc create -f -`
Navigate to your project 'overview' tab in the Openshift web console. Notice that a DeploymentConfig was created. Watch as the number of configured pods are created and become available. 

Repeat this process for each environment.
Setup of your Openshift Kie server is now completed.

<a href="#jenkins-setup"></a>

## Jenkins setup

As mentioned in the [Prerequisistes section](#prerequisites), your Jenkins server should have the Openshift plugin installed. Additionally, worker nodes should have the Openshift CLI (`oc`) and docker installed. 


Once you have your project setup in Openshift, you can begin setting up your Jenkins pipelines. 

Open the `build/Jenkinsfile` file in your favorite text editor. Notice that this Jenkinsfile is parameterized and supports building multiple projects. Find the `DEPLOY_PROJECT` choice parameter. Modify the choices to exactly match the Openshift projects that you want to build. Each line item should be separated with a newline (`\n`) character. Save this file and push this repository to your enterprise Git server. 

Next, Open the `deploy/Jenkinsfile` in your favorite text editor. In the same way as the build Jenkinsfile, edit the `DEPLOY_PROJECT` parameter to match your Openshift project names. Notice the `DEPLOY_ENV` choice parameter. This option will allow you to select which environment you wish to deploy to. The fields here should match the cluster names you configured in Jenkins [in the prerequisites section](#prerequisites). Save these changes and push them to this repository in your enterprise Git server



==

To get started, navigate to your Jenkins homepage. On the right of the screen, click "New Item". Set the name to a meaningful name that a adheres to your organization/project's naming standards. Select "Folder" type, and click "OK". If desired, set a display name and description then click Save. 

In this folder, click "New Item" and name it "Build". Choose "Multibranch Pipeline" type, and click OK. In the configuration page that appears, set the display name and description if desired. Under "Branch Sources", choose "Git" as a source. Project Repository should be the git clone URL for THIS REPO, __NOT your RHPAM project__. If necessary, add credentials for your Git server. All other fields under branch sources should be left as default. Under "Build Configuration", set script path to "build/Jenkinsfile. This will tell Jenkins to choose our build jenkinsfile. Click Save.  The log should say "Finished: SUCCESS" at the end. Click the "Up" button in the top left of the page, then click "Build" --> master. You will likely have a failed build here - this is OK; Jenkins tried to run a build, however as this is a parameterized build, Jenkins did not provide a parameter and the build is complaining aobut this. To verify this works, click "Build with parameters" --> choose DEPLOY_PROJECT field --> Build. You can click on the running build to view the build logs. Jenkins will pipe logs from the build and wait for the build to complete and push the image to the docker registry. In the future if you have any build failures, the execution will appear red and say "Failed". This log is the first place to go to determine what went wrong. 


Next, navigate back to the folder project you created earlier and click "New Item". Similarly to before, name your project "Deploy", choose "Multibranch Pipeline" type, and click OK. Select the branch source type with the same git project repository as before (not the RHPAM project), then select your SCM credentials if necessary. Under the build configuration section, ensure mode is set to Jenkinsfile, and script path should be set to deploy/Jenkinsfile and click Save.

When ready to deploy, navigate to the Deploy job you created, and navigate to the master branch. Click "Build with Parameters" and fill out the configuration options.  Click Build --> <latest build job> --> Console output and watch the logs. Jenkins will wait for the deployment to be completed or fail before the job terminates. Eventually, you should see "Finished: SUCCESS" at the bottom, indicating that your deployment is successfully completed. To verify, run `oc get dc -n my-decisionservice` and see a recent Created time, and the number of configured replicas are running. This can also be done from the Openshift web console instead of the command line. 




<a href="#external-docs"></a>

## External docs:
 - [oc cli](https://docs.openshift.com/container-platform/3.11/cli_reference/get_started_cli.html#cli-reference-get-started-cli) - The preferred way to interact with an Openshift cluster
 - [Quay](https://access.redhat.com/documentation/en-us/red_hat_quay/2.9/) - Quay is a Docker image repository, available in both self-hosted and SaaS options.
 - [Jenkins](https://www.jenkins.io/doc/) - Jenkins is an open-source build and automation server
 - [Docker](https://docs.docker.com/get-docker/) - The container engine that our apps will be built and deployed with
 - [Openshift](https://docs.openshift.com/container-platform/3.11/welcome/index.html) - A container orchestration platform built on top of Kubernetes. 

<!-- Topics: -->
 <!-- - Jenkins setup & configuration x 
 - Likely modifications to Jenkinsfiles & how-to x 
 - Setting up new project (env-setup) x
 - - Setup Quay robot account secret x  -->
 <!-- - Building
 - Deploying builds -->
 <!-- - more ? -->
 
